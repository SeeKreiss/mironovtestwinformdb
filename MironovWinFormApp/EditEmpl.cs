﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MironovWinFormApp
{
    public partial class EditEmpl : Form
    {

        static bool newbie;
        Empl emplForm;
        public EditEmpl(string empID, Empl emplF)   //Если переход был по кнопке "данные сотрудника"
        {
            InitializeComponent();
            NotNewEmp();
            var emps = Program.context.Empoyees.ToList();
            decimal id = Decimal.Parse(empID);
            Empoyee dataEmp = emps.FirstOrDefault(x => x.ID.Equals(id));
            //Заполнение всех текстбоксов
            FirstName.Text = dataEmp.FirstName;
            SurName.Text = dataEmp.SurName;
            Patronymic.Text = dataEmp.Patronymic;
            dateOfBirth.MaxDate = DateTime.Today;
            dateOfBirth.Value = dataEmp.DateOfBirth;
            DocNumber.Text = dataEmp.DocNumber;
            DocSeries.Text = dataEmp.DocSeries;
            Position.Text = dataEmp.Position;            
            ageTB.Text = ageCalc(dataEmp.DateOfBirth).ToString();
            IdTB.Text = empID;
            //заполнение бокса с отделами
            var deps = Program.context.Departments.ToList();
            departCB.DataSource = deps;
            departCB.DisplayMember = "Name";
            departCB.Invalidate();
            departCB.Text = dataEmp.Department.Name;            
            emplForm = emplF;
        }


        public EditEmpl(Empl emplF, string dataEmp)     //Если переход был по кнопке "новый сотрудник"
        {
            InitializeComponent();
            try
            {
                newbie = true;
                ageTB.Text = "0";
                var deps = Program.context.Departments.ToList();
                departCB.DataSource = deps;
                departCB.DisplayMember = "Name";
                departCB.Invalidate();
                departCB.Text = dataEmp;
                dateOfBirth.MaxDate = DateTime.Today;
                NewEmp();
            }
            catch
            {
            }
            emplForm = emplF;

        }


        public int ageCalc(DateTime birth)          //вычисление возраста
        {
            DateTime today = DateTime.Today;
            int age = today.Year - birth.Year;
            if (birth.Date > today.AddYears(-age))
                age--;
            return age;
        }


        private void SaveButton_Click(object sender, EventArgs e) //кнопка сохранить
        {
            CultureInfo provider = CultureInfo.InstalledUICulture;
            var emps = Program.context.Empoyees;
            DateTime dt;
            decimal intIdt = Program.context.Empoyees.Max(u => u.ID);
            if (!string.IsNullOrWhiteSpace(FirstName.Text) && (FirstName.Text.Count() <= 50)     //проверка правильности заполненных полей
            && !string.IsNullOrWhiteSpace(SurName.Text) && (SurName.Text.Count() <= 50)
            && (Patronymic.Text.Count() <= 50) && (DocSeries.Text.Count() <= 4) && (DocNumber.Text.Count() <= 6)
            && !string.IsNullOrWhiteSpace(Position.Text) && (Position.Text.Count() <= 50)
            && (DateTime.TryParseExact(dateOfBirth.Text, "dd-MM-yyyy", provider, DateTimeStyles.None, out dt) && (ageCalc(dt) > 0))
            && !string.IsNullOrWhiteSpace(departCB.Text) && (intIdt < 99999)) 
            {
                if (!newbie)  //если не новый сотрудник
                {
                    decimal id = Decimal.Parse(IdTB.Text);
                    Empoyee dataEmp = emps.FirstOrDefault(x => x.ID.Equals(id));
                    //заполение данных об одном сотруднике
                    dataEmp.FirstName = FirstName.Text;
                    dataEmp.SurName = SurName.Text;
                    dataEmp.Patronymic = Patronymic.Text;
                    dataEmp.DateOfBirth = DateTime.ParseExact(dateOfBirth.Text, "dd-MM-yyyy", provider);
                    dataEmp.DepartmentID = Program.context.Departments.FirstOrDefault(x => x.Name.Equals(departCB.Text)).ID;
                    dataEmp.Position = Position.Text;
                    dataEmp.DocNumber = DocNumber.Text;
                    dataEmp.DocSeries = DocSeries.Text;
                    Program.context.SaveChanges();
                    MessageBox.Show("Сотрудник изменён");
                }
                else
                {
                    Empoyee dataEmp = new Empoyee  //добавление нового сотрудника
                    {
                        FirstName = FirstName.Text,
                        SurName = SurName.Text,
                        Patronymic = Patronymic.Text,
                        DateOfBirth = DateTime.ParseExact(dateOfBirth.Text, "dd-MM-yyyy", provider),
                        DepartmentID = Program.context.Departments.FirstOrDefault(x => x.Name.Equals(departCB.Text)).ID,
                        Position = Position.Text,
                        DocNumber = DocNumber.Text,
                        DocSeries = DocSeries.Text
                    };
                    Program.context.Empoyees.Add(dataEmp);
                    Program.context.SaveChanges();
                    newbie = false;
                    MessageBox.Show("Сотрудник добавлен");
                    IdTB.Text = dataEmp.ID.ToString();
                    NotNewEmp();
                }
            }
            else //Выявление ошибок ввода
            {
                if (FirstName.Text.Count() > 50)
                    MessageBox.Show("Слишком длинное имя");
                if (SurName.Text.Count() > 50)
                    MessageBox.Show("Слишком длинная фамилия");
                if (Patronymic.Text.Count() > 50)
                    MessageBox.Show("Слишком длинное отчетво");
                if (DocSeries.Text.Count() > 4)
                    MessageBox.Show("Слишком длинная серия документа");
                if (DocNumber.Text.Count() > 6)
                    MessageBox.Show("Слишком длинный номер документа");
                if (Position.Text.Count() > 50)
                    MessageBox.Show("Слишком длинное название должности");
                if (Int32.TryParse(ageTB.Text, out int a) || a < 1) 
                    MessageBox.Show("Проверьте дату рождения");
                if (intIdt == 99999)
                    MessageBox.Show("Слишком много сотрудников");
                MessageBox.Show("Проверьте правильность ввода данных");
            }
            
        }


        private void EditEmpl_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseF();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            CloseF();
        }


        private void CloseF()  //закрытие окна
        {
            newbie = false;
            emplForm.Show();            
            this.Dispose();
            this.Close();
            ReloadEditEmpl();
        }

        private void NewEmpl_Click(object sender, EventArgs e)      //кнопка добавление сотрудника
        {
            //очистка полей
            FirstName.Clear();
            SurName.Clear();
            Patronymic.Clear();
            dateOfBirth.Value = DateTime.Today;
            DocNumber.Clear();
            DocSeries.Clear();
            Position.Clear();
            ageTB.Clear();
            IdTB.Clear();
            newbie = true;
            NewEmp();
        }


        private void dateOfBirth_ValueChanged(object sender, EventArgs e)       //обновление возраста при изменении даты рождения
        {
            CultureInfo provider = CultureInfo.InstalledUICulture;
            DateTime dt;
            if (DateTime.TryParseExact(dateOfBirth.Text, "dd-MM-yyyy", provider, DateTimeStyles.None, out dt))
                ageTB.Text = ageCalc(dt).ToString();
        }


        public event Action ReloadEditEmpl;     //ивент при закрытии формы


        private void NotNewEmp()
        {
            NewEmpl.Show();
            NewEmpl.Enabled = true;
            Newlabel.Show();
            Newlabel.Enabled = true;
        }


        private void NewEmp()
        {
            NewEmpl.Hide();
            NewEmpl.Enabled = false;
            Newlabel.Hide();
            Newlabel.Enabled = false;
        }

    }
}

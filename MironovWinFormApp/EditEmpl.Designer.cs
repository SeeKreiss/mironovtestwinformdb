﻿namespace MironovWinFormApp
{
    partial class EditEmpl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditEmpl));
            this.FirstName = new System.Windows.Forms.TextBox();
            this.SurName = new System.Windows.Forms.TextBox();
            this.Patronymic = new System.Windows.Forms.TextBox();
            this.Position = new System.Windows.Forms.TextBox();
            this.DocSeries = new System.Windows.Forms.TextBox();
            this.DocNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ageTB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.departCB = new System.Windows.Forms.ComboBox();
            this.dateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.SaveButton = new System.Windows.Forms.Button();
            this.BackButton = new System.Windows.Forms.Button();
            this.IdTB = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.NewEmpl = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.Newlabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // FirstName
            // 
            resources.ApplyResources(this.FirstName, "FirstName");
            this.FirstName.Name = "FirstName";
            // 
            // SurName
            // 
            resources.ApplyResources(this.SurName, "SurName");
            this.SurName.Name = "SurName";
            // 
            // Patronymic
            // 
            resources.ApplyResources(this.Patronymic, "Patronymic");
            this.Patronymic.Name = "Patronymic";
            // 
            // Position
            // 
            resources.ApplyResources(this.Position, "Position");
            this.Position.Name = "Position";
            // 
            // DocSeries
            // 
            resources.ApplyResources(this.DocSeries, "DocSeries");
            this.DocSeries.Name = "DocSeries";
            // 
            // DocNumber
            // 
            resources.ApplyResources(this.DocNumber, "DocNumber");
            this.DocNumber.Name = "DocNumber";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // ageTB
            // 
            resources.ApplyResources(this.ageTB, "ageTB");
            this.ageTB.Name = "ageTB";
            this.ageTB.ReadOnly = true;
            this.ageTB.TabStop = false;
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // departCB
            // 
            this.departCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.departCB.FormattingEnabled = true;
            resources.ApplyResources(this.departCB, "departCB");
            this.departCB.Name = "departCB";
            // 
            // dateOfBirth
            // 
            resources.ApplyResources(this.dateOfBirth, "dateOfBirth");
            this.dateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateOfBirth.Name = "dateOfBirth";
            this.dateOfBirth.ValueChanged += new System.EventHandler(this.dateOfBirth_ValueChanged);
            // 
            // SaveButton
            // 
            resources.ApplyResources(this.SaveButton, "SaveButton");
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // BackButton
            // 
            resources.ApplyResources(this.BackButton, "BackButton");
            this.BackButton.Name = "BackButton";
            this.BackButton.UseVisualStyleBackColor = true;
            this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
            // 
            // IdTB
            // 
            resources.ApplyResources(this.IdTB, "IdTB");
            this.IdTB.Name = "IdTB";
            this.IdTB.ReadOnly = true;
            this.IdTB.TabStop = false;
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // NewEmpl
            // 
            resources.ApplyResources(this.NewEmpl, "NewEmpl");
            this.NewEmpl.Name = "NewEmpl";
            this.NewEmpl.UseVisualStyleBackColor = true;
            this.NewEmpl.Click += new System.EventHandler(this.NewEmpl_Click);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // Newlabel
            // 
            resources.ApplyResources(this.Newlabel, "Newlabel");
            this.Newlabel.Name = "Newlabel";
            // 
            // EditEmpl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Newlabel);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.NewEmpl);
            this.Controls.Add(this.BackButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.dateOfBirth);
            this.Controls.Add(this.departCB);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DocNumber);
            this.Controls.Add(this.DocSeries);
            this.Controls.Add(this.ageTB);
            this.Controls.Add(this.IdTB);
            this.Controls.Add(this.Position);
            this.Controls.Add(this.Patronymic);
            this.Controls.Add(this.SurName);
            this.Controls.Add(this.FirstName);
            this.Name = "EditEmpl";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EditEmpl_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox FirstName;
        private System.Windows.Forms.TextBox SurName;
        private System.Windows.Forms.TextBox Patronymic;
        private System.Windows.Forms.TextBox Position;
        private System.Windows.Forms.TextBox DocSeries;
        private System.Windows.Forms.TextBox DocNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ageTB;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox departCB;
        private System.Windows.Forms.DateTimePicker dateOfBirth;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button BackButton;
        private System.Windows.Forms.TextBox IdTB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button NewEmpl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label Newlabel;
    }
}
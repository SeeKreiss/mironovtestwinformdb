﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace MironovWinFormApp
{
    public partial class Empl : Form
    {
        static Guid dep;
        StartWindow start;
        List<Empoyee> emps;
        public Empl(String value, Guid depID, StartWindow strt)
        {
            InitializeComponent();
            var deps = Program.context.Departments.ToList();
            emps = Program.context.Empoyees.ToList();
            dep = depID;
            comboBox1.DataSource = deps;
            comboBox1.DisplayMember = "Name";
            comboBox1.Invalidate();
            comboBox1.Text = value;
            start = strt;
        }


        private void dataGridFilter(string depName)     //заполнение таблицы сотрудников выбранного отдела
        {
            dataGridView1.Rows.Clear();
            var filteredEmps = emps.Where(x => x.Department.Name.Equals(depName)).ToList();
            foreach (var emp in filteredEmps)
            {
                dataGridView1.Rows.Add(emp.ID, emp.FirstName, emp.SurName, emp.Patronymic, emp.Position);
            }
        }


        private void comboBox1_TextChanged(object sender, EventArgs e)      //обновление таблицы при изменении комбобокса
        {
            string depo = comboBox1.Text;
            dataGridFilter(depo);
        }


        private void BackButton_Click(object sender, EventArgs e)    
        {
            try
            {
                CloseF();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void EmplButton_Click(object sender, EventArgs e)       //кнопка просмотра данных сотрудника
        {
            if (dataGridView1.SelectedCells.Count > 0)
            {
                string empID = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                comboBox1.Text = emps.FirstOrDefault(x => x.ID == Decimal.Parse(empID)).Department.Name;
                EditEmpl editEmpl = new EditEmpl(empID, this);

                editEmpl.Show();
                editEmpl.ReloadEditEmpl += Reload;
                this.Hide();
            }
            else
            {
                MessageBox.Show("Выберите сотрудника");
            }

        }

        private void Empl_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseF();
        }

        private void LookAllbutton_Click(object sender, EventArgs e)        //кнопка просмотр всех сотрудников
        {
            dataGridView1.Rows.Clear();
            foreach (var emp in emps)
            {
                dataGridView1.Rows.Add(emp.ID, emp.FirstName, emp.SurName, emp.Patronymic, emp.Position);
            }            
            LookAtAll();
        }


        private void DeleteButton_Click(object sender, EventArgs e)             //кнопка удаления сотрудника
        {
            if (dataGridView1.SelectedCells.Count > 0)
            {
                int empID = Int32.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                var delEmpl = emps.FirstOrDefault(x => x.ID == empID);
                string message = "Вы хотите чтобы сотрудник " + Environment.NewLine + delEmpl.FirstName + " "
                    + delEmpl.SurName + " " + delEmpl.Patronymic + Environment.NewLine + "был удален из базы данных?";
                DelYesNo delYesNo = new DelYesNo(message, empID);
                delYesNo.Show();
                delYesNo.ReloadDelYesNo += Reload;
            }
            else
            {
                MessageBox.Show("Выберите сотрудника");
            }
        }


        void Reload()       //метод обновления таблицы 
        {
            dataGridView1.Rows.Clear();
            emps = Program.context.Empoyees.ToList();
            foreach (var emp in emps)
            {
                dataGridView1.Rows.Add(emp.ID, emp.FirstName, emp.SurName, emp.Patronymic, emp.Position);
            }
            LookAtAll();
        }


        private void Look1_Click(object sender, EventArgs e)        //Кнопка просмотра сотрудников по отделам
        {            
            LookAt1();
            string depo = comboBox1.Text;
            dataGridFilter(depo);
        }


        private void LookAtAll()           //замена кнопки просмотра сотрудников и комбобокса 
        {
            LookAllButton1.Hide();
            LookAllButton1.Enabled = false;
            comboBox1.Hide();
            comboBox1.Enabled = false;
            label1.Show();
            label1.Enabled = true;
            Look1.Show();
            Look1.Enabled = true;
        }


        private void LookAt1()             //замена лэйбла и кнопки просмотра по отделам
        {
            LookAllButton1.Show();
            LookAllButton1.Enabled = true;
            comboBox1.Show();
            comboBox1.Enabled = true;
            label1.Hide();
            label1.Enabled = false;
            Look1.Hide();
            Look1.Enabled = false;
        }


        private void Newbie_Click(object sender, EventArgs e)       //Кнопка добавление сотрудника
        {
            string depName = comboBox1.Text;
            EditEmpl editEmpl = new EditEmpl(this, depName);
            editEmpl.Show();
            editEmpl.ReloadEditEmpl += Reload;
            this.Hide();
        }


        private void CloseF()       //закрытие формы
        {
            start.Show();
            this.Dispose();
            this.Close();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MironovWinFormApp
{
    public partial class StartWindow : Form
    {
        static Dictionary<string, Guid> dict = new Dictionary<string, Guid>(); //хранит отдел и его ID
        //Необходимые для заполения дерева параметры
        List<Department> deps;
        string rootName;
        Guid parentID;
        string childName;
        Guid childID;

        public StartWindow()
        {
            InitializeComponent();
            dict.Clear();
            deps = Program.context.Departments.ToList();

            foreach (var dep in deps)           //Ищет отдел без родителя и запоняет словарь отделов
            {
                if (dep.ParentDepartmentID == null)
                {
                    parentID = dep.ID;
                    rootName = dep.Name;
                }
                dict.Add(dep.Name, dep.ID);
            }
            treeView1.Nodes.Add(CreateNode(rootName, parentID));
            treeView1.ExpandAll();
           
        }


        private TreeNode CreateNode(string parentName, Guid parent)
        {
            var newNode = new TreeNode(parentName);

            foreach (var i in deps)         //поиск дочерних отделов в базе
            {
                if (i.ParentDepartmentID.Equals(parent))
                {
                    childName = i.Name;         
                    childID = i.ID;
                    newNode.Nodes.Add(CreateNode(childName, childID));      //создание дочерних отделов в дереве
                }                
            }
            return newNode;
        }


        private void ToEmployee_Click(object sender, EventArgs e)       //кнопка просмотра сотрудников отдела
        {
            if (treeView1.SelectedNode.IsSelected)
            {
                
                string DepName = treeView1.SelectedNode.Text;
                Empl empl = new Empl(DepName, dict[DepName], this);
                empl.Show();
                this.Hide();
            }
            else
                MessageBox.Show("Выберите отдел");
        }


        private void StartWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseF();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            CloseF();
        }

        private void CloseF()       //закрытие формы
        {
            Program.context.Dispose();
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MironovWinFormApp
{
    public partial class DelYesNo : Form
    {
        int EmpID;
        public DelYesNo(string message, int ID)
        {
            InitializeComponent();
            label1.Text = message;
            EmpID = ID;
        }


        private void YesButton_Click(object sender, EventArgs e)
        {
            var delEmpl = Program.context.Empoyees.FirstOrDefault(x => x.ID == EmpID);
            Program.context.Empoyees.Remove(delEmpl);
            Program.context.SaveChanges();
            ReloadDelYesNo();
            this.Hide();
            MessageBox.Show("Сотрудник был удалён");
            this.Close();
        }


        public event Action ReloadDelYesNo;


        private void NoButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
